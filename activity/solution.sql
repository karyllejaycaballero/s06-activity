-- Karylle Jay H. Caballero
-- S06 Activity

/*

------------------Answers---------------------

2.
a. 
BU1032 - The Busy Executive's Database Guide
BU2075 - You Can Combat Computer Stress!

b.
BU1111 - Cooking with Computers

c.
213-46-8915 - Green, Marjorie
409-56-7008 - Bennet, Abraham

d.
1389 - Algodata Infosystems

e.
BU1032 - The Busy Executive's Database Guide
BU1111 - Cooking with Computers
BU7832 - Straight Talk About Computers
PC1035 - But Is It User Friendly?
PC8888 - Secrets of Silicon Valley
PC9999 - Net Etiquette

------------------Answers---------------------
*/

CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY(id)
); 

CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY (id),
CONSTRAINT fk_posts_user_id
	FOREIGN KEY (author_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
user_id INT NOT NULL,
post_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY (id),
CONSTRAINT fk_post_likes_post_id
	FOREIGN KEY (post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT fk_post_likes_user_id
	FOREIGN KEY (user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT,
user_id INT NOT NULL,
post_id INT NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY (id),
CONSTRAINT fk_post_comments_post_id
	FOREIGN KEY (post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT fk_post_comments_user_id
	FOREIGN KEY (user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);
